//! See https://prometheus.io/docs/alerting/latest/configuration/#webhook_config for the format
//! which is posted by Prometheus

use std::collections::HashMap;
use std::convert::TryInto as _;

use chrono::{DateTime, FixedOffset};
use serde::de;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use tera::{Context, Tera};

use crate::error::BotConfigError;

pub const TEMPLATE_NAME_PLAIN: &str = "plaintext.tera";
pub const TEMPLATE_NAME_HTML: &str = "html.tera";

#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct WebhookData {
    version: String,
    group_key: String, // key identifying the group of alerts (e.g. to deduplicate)
    truncated_alerts: i32, // how many alerts have been truncated due to "max_alerts"
    status: Status,
    receiver: String,
    group_labels: HashMap<String, String>,
    common_labels: HashMap<String, String>,
    common_annotations: HashMap<String, String>,
    #[serde(rename = "externalURL")]
    external_url: String, // backlink to the Alertmanager
    alerts: Vec<Alert>,
}
impl WebhookData {
    pub fn render_plain(&self, templating: &Tera) -> Result<String, BotConfigError> {
        let context = Context::from_serialize(self)?;
        Ok(templating.render(TEMPLATE_NAME_PLAIN, &context)?)
    }

    pub fn render_html(&self, templating: &Tera) -> Result<String, BotConfigError> {
        let context = Context::from_serialize(self)?;
        Ok(templating.render(TEMPLATE_NAME_HTML, &context)?)
    }

    pub fn alert_count(&self) -> Result<i64, Box<dyn std::error::Error>> {
        Ok(self.alerts.len().try_into()?)
    }

    #[cfg(test)]
    pub fn new(
        version: String,
        group_key: String,
        truncated_alerts: i32,
        status: Status,
        receiver: String,
        group_labels: HashMap<String, String>,
        common_labels: HashMap<String, String>,
        common_annotations: HashMap<String, String>,
        external_url: String,
        alerts: Vec<Alert>,
    ) -> Self {
        WebhookData {
            version,
            group_key,
            truncated_alerts,
            status,
            receiver,
            group_labels,
            common_labels,
            common_annotations,
            external_url,
            alerts,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Alert {
    status: Status,
    labels: HashMap<String, String>,
    annotations: HashMap<String, String>,
    #[serde(
        serialize_with = "serialize_datetime",
        deserialize_with = "deserialize_datetime"
    )]
    starts_at: DateTime<FixedOffset>,
    #[serde(
        serialize_with = "serialize_datetime",
        deserialize_with = "deserialize_datetime"
    )]
    ends_at: DateTime<FixedOffset>,
    #[serde(rename = "generatorURL")]
    generator_url: String, // identifies the entity that caused the alert
}
#[cfg(test)]
impl Alert {
    pub fn new(
        status: Status,
        labels: HashMap<String, String>,
        annotations: HashMap<String, String>,
        starts_at: DateTime<FixedOffset>,
        ends_at: DateTime<FixedOffset>,
        generator_url: String,
    ) -> Self {
        Alert {
            status,
            labels,
            annotations,
            starts_at,
            ends_at,
            generator_url,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    Resolved,
    Firing,
}

fn serialize_datetime<S>(datetime: &DateTime<FixedOffset>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&datetime.to_rfc3339())
}

fn deserialize_datetime<'de, D>(deserializer: D) -> Result<DateTime<FixedOffset>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    DateTime::parse_from_rfc3339(&s).map_err(de::Error::custom)
}
