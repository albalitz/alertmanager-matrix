use matrix_sdk;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum BotConfigError {
    #[error("environment variable not set: {0}")]
    Environment(String),
    #[error("template file missing - expected: {expected:?}; found: {found:?}")]
    MissingTemplate {
        expected: Vec<String>,
        found: Vec<String>,
    },
    #[error("invalid message template: {0}")]
    InvalidMessageTemplate(#[from] tera::Error),
    #[error("can't locate config directory")]
    ConfigDirDoesNotExist,
    #[error("can't create matrix bot config directory")]
    ConfigDirNotCreated(#[from] std::io::Error),
    #[error("invalid homeserver URL")]
    HomeserverUrl,
    #[error("authentication with matrix failed: {0}")]
    Auth(#[from] matrix_sdk::Error),
    #[error("invalid room ID")]
    InvalidRoomId,
}
