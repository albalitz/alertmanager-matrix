#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate prometheus;
#[macro_use]
extern crate rocket;

mod alertmanager;
mod bot;
mod error;
mod metrics;

use clap::{crate_description, crate_name, crate_version};
use clap::{App, Arg};
use prometheus::{Encoder as _, TextEncoder};
use tokio::runtime::Runtime;
use tracing::error;
use tracing::Level;
use tracing_subscriber::FmtSubscriber;

use rocket::http::Status;
use rocket::response::status;
use rocket::State;
use rocket_contrib::json::Json;

use alertmanager::WebhookData;
use bot::Bot;
use metrics::{ErrorTypes, ERRORS};

#[post("/webhook", data = "<webhook_data>")]
fn alert_webhook(
    bot: State<Bot>,
    webhook_data: Json<WebhookData>,
) -> Result<(), status::Custom<String>> {
    match Runtime::new()
        .expect("Error creating tokio runtime when handling webhook")
        .block_on(bot.alert(webhook_data.into_inner()))
    {
        Ok(_) => Ok(()),
        Err(e) => {
            error!("Error alerting the matrix: {}", e);
            ERRORS
                .with_label_values(&[&ErrorTypes::Matrix.to_string()])
                .inc();
            Err(status::Custom(Status::InternalServerError, e.to_string()))
        }
    }
}

#[get("/metrics")]
fn metrics() -> Result<String, status::Custom<String>> {
    let encoder = TextEncoder::new();
    let metric_families = prometheus::gather();
    let mut buffer = vec![];
    encoder.encode(&metric_families, &mut buffer).unwrap();

    match String::from_utf8(buffer) {
        Ok(metrics) => Ok(metrics),
        Err(e) => {
            error!("Error exporting metrics: {}", e);
            ERRORS
                .with_label_values(&[&ErrorTypes::Metrics.to_string()])
                .inc();
            Err(status::Custom(Status::InternalServerError, e.to_string()))
        }
    }
}

/// Create a rocket that can be used in tests
///
/// Note: managed things need to be added afterwards to keep this functions as flexible as possible
fn prepare_rocket() -> rocket::Rocket {
    rocket::ignite().mount("/", routes![alert_webhook, metrics])
}

#[tokio::main]
async fn main() {
    let args = App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .multiple(true)
                .help("Make output more verbose")
                .long_help("Make output more verbose. Pass multiple times to increase verbosity. Default: WARN"),
        )
        .get_matches();

    let verbosity = match args.occurrences_of("verbosity") {
        0 => Level::WARN,
        1 => Level::INFO,
        2 => Level::DEBUG,
        _ => Level::TRACE,
    };

    let subscriber = FmtSubscriber::builder().with_max_level(verbosity).finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    metrics::register();
    metrics::reset_counters();

    let bot = match Bot::from_env().await {
        Ok(bot) => bot,
        Err(e) => {
            eprintln!("Error configuring matrix bot: {}", e);
            return;
        }
    };

    prepare_rocket().manage(bot).launch();
}

#[cfg(test)]
mod test_metrics_output {
    use rocket::http::Status;
    use rocket::local::Client;

    use super::*;
    use crate::metrics;

    #[test]
    fn should_export_initial_values_for_metrics_after_register_and_reset() {
        metrics::register();
        metrics::reset_counters();

        let rocket = prepare_rocket();
        let client = Client::new(rocket).expect("Expected valid rocket instance");
        let mut response = client.get("/metrics").dispatch();

        assert_eq!(response.status(), Status::Ok);
        let response_body: String = response.body_string().expect("Expected response body");
        for expected_metric_name in vec![
            "alertmanager_matrix_messages_posted",
            "alertmanager_matrix_alerts_posted",
        ] {
            assert!(
                response_body.contains(&format!("{} 0", expected_metric_name)),
                format!(
                    "Response should contain metric name: {}",
                    expected_metric_name
                )
            );
        }
        // this is not tested in the loop as the exact syntax differs slightly due to labels
        for expected_type_label in vec![
            metrics::ErrorTypes::Matrix.to_string(),
            metrics::ErrorTypes::Metrics.to_string(),
        ] {
            let expected_metric_label_string = format!(
                r#"alertmanager_matrix_errors{{type="{}"}} 0"#,
                expected_type_label
            );
            assert!(
                response_body.contains(&expected_metric_label_string),
                format!(
                    "Response should contain metric name and label: {}",
                    expected_metric_label_string
                )
            );
        }
    }
}
