use std::convert::TryFrom;
use std::env;
use std::fs;
use std::path::PathBuf;

use dirs;
use matrix_sdk::events::room::message::{MessageEventContent, TextMessageEventContent};
use matrix_sdk::identifiers::RoomId;
use matrix_sdk::{Client, ClientConfig, SyncSettings};
use tera::Tera;
use url::Url;

use crate::alertmanager::WebhookData;
use crate::alertmanager::{TEMPLATE_NAME_HTML, TEMPLATE_NAME_PLAIN};
use crate::error::BotConfigError;
use crate::metrics::{ALERTS_POSTED, MESSAGES_POSTED};

const ENV_BOT_USERNAME: &str = "ALERTMANAGER_MATRIX_BOT_USERNAME";
const ENV_BOT_DISPLAY_NAME: &str = "ALERTMANAGER_MATRIX_BOT_DISPLAY_NAME";
const DEFAULT_BOT_DISPLAY_NAME: &str = "Prometheus";
const ENV_BOT_PASSWORD: &str = "ALERTMANAGER_MATRIX_BOT_PASSWORD";
const ENV_BOT_HOMESERVER: &str = "ALERTMANAGER_MATRIX_BOT_HOMESERVER";
const ENV_BOT_ROOM: &str = "ALERTMANAGER_MATRIX_BOT_ROOM";
const ENV_MESSAGE_TEMPLATE_FILES: &str = "ALERTMANAGER_MATRIX_TEMPLATE_FILES"; // TODO: CLI arg?

const CONFIG_DIR_NAME: &str = "prometheus-alerting-matrix-bot";

#[derive(Debug, Clone)]
pub struct Bot {
    client: Client,
    room_id: RoomId,
    templating: Tera,
}
impl Bot {
    pub async fn from_env() -> Result<Self, BotConfigError> {
        let message_template_files = match env::var(ENV_MESSAGE_TEMPLATE_FILES) {
            Ok(f) => f,
            Err(_) => {
                return Err(BotConfigError::Environment(String::from(
                    ENV_MESSAGE_TEMPLATE_FILES,
                )))
            }
        };
        let templating = Tera::new(&message_template_files)?;
        let expected_templates = vec![TEMPLATE_NAME_PLAIN, TEMPLATE_NAME_HTML];
        if !expected_templates
            .iter()
            .all(|&template| templating.templates.contains_key(template))
        {
            let expected: Vec<String> = expected_templates.iter().map(|t| t.to_string()).collect();
            let found: Vec<String> = templating.templates.keys().map(|k| k.to_string()).collect();
            return Err(BotConfigError::MissingTemplate { expected, found });
        }

        let username = match env::var(ENV_BOT_USERNAME) {
            Ok(username) => username,
            Err(_) => return Err(BotConfigError::Environment(String::from(ENV_BOT_USERNAME))),
        };
        let display_name =
            env::var(ENV_BOT_DISPLAY_NAME).unwrap_or(String::from(DEFAULT_BOT_DISPLAY_NAME));
        let password = match env::var(ENV_BOT_PASSWORD) {
            Ok(password) => password,
            Err(_) => return Err(BotConfigError::Environment(String::from(ENV_BOT_PASSWORD))),
        };
        let homeserver = match env::var(ENV_BOT_HOMESERVER) {
            Ok(homeserver) => homeserver,
            Err(_) => {
                return Err(BotConfigError::Environment(String::from(
                    ENV_BOT_HOMESERVER,
                )))
            }
        };
        let room_id = match env::var(ENV_BOT_ROOM) {
            Ok(room_id) => room_id,
            Err(_) => return Err(BotConfigError::Environment(String::from(ENV_BOT_ROOM))),
        };

        let config_dir: PathBuf = match dirs::config_dir() {
            Some(mut cd) => {
                cd.push(CONFIG_DIR_NAME);
                cd
            }
            None => return Err(BotConfigError::ConfigDirDoesNotExist),
        };
        fs::create_dir_all(&config_dir)?;
        let client_config = ClientConfig::new().store_path(config_dir);
        let homeserver_url = match Url::parse(&homeserver) {
            Ok(u) => u,
            Err(_) => return Err(BotConfigError::HomeserverUrl),
        };
        let client = Client::new_with_config(homeserver_url, client_config).unwrap();
        client
            .login(username, password, None, Some(display_name))
            .await?;
        client.sync(SyncSettings::default()).await?;

        let room_id = match RoomId::try_from(room_id) {
            Ok(room_id) => room_id,
            Err(_) => return Err(BotConfigError::InvalidRoomId),
        };

        Ok(Self {
            client,
            room_id,
            templating,
        })
    }

    pub async fn alert(&self, webhook_data: WebhookData) -> Result<(), Box<dyn std::error::Error>> {
        let content = MessageEventContent::Text(TextMessageEventContent {
            body: webhook_data.render_plain(&self.templating)?,
            formatted_body: Some(webhook_data.render_html(&self.templating)?),
            format: Some(String::from("org.matrix.custom.html")),
            relates_to: None,
        });
        self.client.room_send(&self.room_id, content, None).await?;
        MESSAGES_POSTED.inc();
        ALERTS_POSTED.inc_by(webhook_data.alert_count()?); // this may result in inaccurate metrics if this fails
        Ok(())
    }
}
