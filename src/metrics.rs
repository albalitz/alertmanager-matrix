use std::fmt;

use prometheus::{default_registry, IntCounter, IntCounterVec, Opts};
use tracing::info;

lazy_static! {
    pub static ref MESSAGES_POSTED: IntCounter = register_int_counter!(
        "alertmanager_matrix_messages_posted",
        "Number of messages posted to the matrix room"
    )
    .unwrap();
    pub static ref ALERTS_POSTED: IntCounter = register_int_counter!(
        "alertmanager_matrix_alerts_posted",
        "Number of alerts posted to the matrix room"
    )
    .unwrap();
    pub static ref ERRORS: IntCounterVec = IntCounterVec::new(
        Opts::new(
            "alertmanager_matrix_errors",
            "Number of errors encountered since startup"
        ),
        &["type"]
    )
    .unwrap();
}

pub enum ErrorTypes {
    Matrix,
    Metrics,
}
impl fmt::Display for ErrorTypes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ErrorTypes::Matrix => write!(f, "matrix"),
            ErrorTypes::Metrics => write!(f, "metrics_export"),
        }
    }
}

/// Resetting counters to 0 on startup ensures there's content in the /metrics response
pub fn reset_counters() {
    info!("Resetting exported counters to 0...");
    MESSAGES_POSTED.reset();
    ALERTS_POSTED.reset();
    ERRORS
        .with_label_values(&[&ErrorTypes::Matrix.to_string()])
        .reset();
    ERRORS
        .with_label_values(&[&ErrorTypes::Metrics.to_string()])
        .reset();
}

pub fn register() {
    info!("Registering metrics...");
    default_registry()
        .register(Box::new(ERRORS.clone()))
        .unwrap();
}
