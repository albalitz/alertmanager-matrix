# Alertmanager Matrix
[Prometheus Alerting webhook](https://prometheus.io/docs/alerting/latest/configuration/#webhook_config) receiver that posts your alerts to a [Matrix](https://matrix.org/) room

## Configuration
### Bot
Configuration for authenticating with Matrix and to which room to send messages is done with environment variables:
- `ALERTMANAGER_MATRIX_BOT_USERNAME`  
  e.g. `@example-bot:example.com`
- `ALERTMANAGER_MATRIX_BOT_PASSWORD`
- `ALERTMANAGER_MATRIX_BOT_HOMESERVER`  
  e.g. `https://matrix.example.com`
- `ALERTMANAGER_MATRIX_BOT_ROOM`  
  the _internal room ID_ as shown in the _Advanced_ section of a room's settings in Element

### Message Templating
Configuration of the message content posted to your Matrix room is done using [Tera](https://tera.netlify.app/) templates.
To specify which templates to use, set the `ALERTMANAGER_MATRIX_TEMPLATE_FILES` environment variable to a glob matching your desired templates, e.g. `/var/alerting/templates/*.tera`.  
Two template files are expected to match the glob and will be used to render messages: `plaintext.tera` and `html.tera`.

Note that due to the way [Tera collects template files](https://docs.rs/tera/1.5.0/tera/struct.Tera.html#method.new) this needs to contain a `*`.

For example templates, see the files in the example-templates directory.  
Everything sent to webhook receivers from your Alertmanager as documented in the [webhook documentation](https://prometheus.io/docs/alerting/latest/configuration/#webhook_config) is available in your templates.

### Rocket
See Rocket's [configuration section](https://rocket.rs/v0.4/guide/configuration/#environment-variables) to see how to configure things like the port and address using environment variables.

### Logging
You can increase the verbosity of the logging output using the `-v` flag.
Check the output of `--help` for more information.

### Prometheus Alertmanager
See the [Alertmanager configuration](https://prometheus.io/docs/alerting/latest/configuration/) docs for detailed instructions.

Example config:
```yaml
route:
  group_by: ['alertname']
  receiver: 'matrix-webhook'

receivers:
  - name: 'matrix-webhook'
    webhook_configs:
      - url: http://localhost:8000/webhook
        send_resolved: true
```

## Deployment
There's a docker image available as [`registry.gitlab.com/albalitz/alertmanager-matrix`](https://gitlab.com/albalitz/alertmanager-matrix/container_registry).  
To use it, you'll need to pass all of the environment variables mentioned above, but if you don't pass `ALERTMANAGER_MATRIX_TEMPLATE_FILES`, the examples from the example-templates directory are used as a default.

### Docker Example
```
# alertmanager-matrix.env
ALERTMANAGER_MATRIX_BOT_USERNAME=@example-bot:example.com
ALERTMANAGER_MATRIX_BOT_PASSWORD=supersecure
ALERTMANAGER_MATRIX_BOT_HOMESERVER=https://matrix.example.com
ALERTMANAGER_MATRIX_BOT_ROOM=!internalroomid:matrix.example.com
```

```bash
docker run --name alertmanager-matrix --env-file alertmanager-matrix.env -p 8000:8000 registry.gitlab.com/albalitz/alertmanager-matrix
```
