FROM rustlang/rust:nightly

ENV ALERTMANAGER_MATRIX_TEMPLATE_FILES /alertmanager-matrix/example-templates/*.tera
WORKDIR /alertmanager-matrix
COPY . /alertmanager-matrix

RUN cargo build --release && \
    mv target/release/alertmanager-matrix /opt/alertmanager-matrix && \
    rm -rf target

ENTRYPOINT ["/opt/alertmanager-matrix"]
